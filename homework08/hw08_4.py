import time
import math
import datetime

#  4.1
def hours_since_epoch() -> float:
    in_secs = time.time()
    in_mins = in_secs / 60
    in_hours = in_mins / 60
    return in_hours


print("Hours since epoch = ", hours_since_epoch())

#  4.2
def co2_inc_per_hour(start: float = 325.03, end: float = 417.04) -> float:
    co2_delta = end - start
    return co2_delta / hours_since_epoch()


print("Rate per hour is: ", co2_inc_per_hour())


#  4.3
def year_to_hours(wanted_year: int) -> int:
    time_later = datetime.datetime(
        wanted_year,
        1,
        1,
        1,
    ).timestamp()
    seconds = time_later
    minutes = seconds / 60
    hours = minutes / 60
    return hours


def projected_co2_level(projection_year: int = 2100) -> float:
    if projection_year < 1971:
        raise ValueError(
            f"Year should be bigger than 1971. Entered was: {projection_year}"
        )
    c0: float = 325.03
    time_delta: int = year_to_hours(projection_year)
    co2_rate: float = co2_inc_per_hour()
    return c0 + (co2_rate * time_delta)


print("Projections for 2022 are: ", projected_co2_level(2022))
print("Projections for 2100 are: ", projected_co2_level())


#  4.4
def radiative_forcing(c1: float = projected_co2_level()) -> float:
    c0: float = 325.03
    alpha: float = 5.35
    return alpha * math.log(c1 / c0)


def delta_T(delta_F: float = radiative_forcing()) -> float:
    l_lambda: float = 0.5
    return delta_F * l_lambda


print("Delta T until 2100 is: ", delta_T())


#  4.5
def predict_increase(year: int) -> float:
    c02_prediction = projected_co2_level(year)
    delta_F = radiative_forcing(c02_prediction)
    return delta_T(delta_F)


print("Delta T for 2300 is: ", predict_increase(2300))
