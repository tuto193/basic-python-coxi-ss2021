import numpy as np
import random
import time

#  3.1
def rand_change(array: list) -> list:
    ret_array = array
    to_add = random.randint(1, 3)
    abs_index = random.randint(0, 8)
    row = abs_index // 3
    col = abs_index % 3
    ret_array[row][col] = to_add
    return ret_array


#  3.2
def get_diff(l1: list, l2: list) -> int:
    total = 0
    for i_r, row in enumerate(l1):
        for i_c, val in enumerate(row):
            if val != l2[i_r][i_c]:
                total += 1
    return total


#  3.3
arr_1 = np.ones((3, 3))
arr_2 = arr_1 + 2

list_1 = arr_1.tolist()
list_2 = arr_2.tolist()

start = time.time()
while get_diff(list_1, list_2) != 0:
    list_1 = rand_change(list_1)
    list_2 = rand_change(list_2)
total = time.time() - start
print(f"It took {total} seconds.")
to_show1 = np.array(list_1)
to_show2 = np.array(list_2)
print(f"List 1 looks like \n{to_show1} \nand 2 like \n{to_show2}.")
