import os


def create_folders(structure: list) -> None:
    root_dir = os.getcwd()
    parent_dir = ""
    for base_dir in structure:
        if type(base_dir) is str:
            if not os.path.exists(base_dir):
                os.mkdir(base_dir)
            parent_dir = base_dir
        else:
            os.chdir(parent_dir)
            for sub_folder in base_dir:
                os.mkdir(sub_folder)
            os.chdir(root_dir)


test_struct = ["a", ["a_1", "a_2"], "b", ["b_1", "b_2"], "c", ["c_1", "c_2"]]
create_folders(test_struct)
