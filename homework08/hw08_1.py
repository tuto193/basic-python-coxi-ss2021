import time
import os
import math

# 1.1
print(time.time())

# 1.2
def make_absolute(rel_path: str) -> str:
    # current working dir
    cwd = os.getcwd()
    os.chdir(rel_path)
    abs_path = os.getcwd()
    os.chdir(cwd)
    return abs_path


# 1.3
def calculate_x(n: int) -> float:
    if n < 1 or n > 12:
        return -1
    return math.sin((n * math.pi) / 6) * 3


def calculate_y(n: int) -> float:
    if n < 1 or n > 12:
        return -1
    return math.cos((n * math.pi) / 6) * 3
