GRID = [
    [7, 0, 0, 4, 0, 0, 1, 2, 0],
    [6, 0, 0, 0, 7, 5, 0, 0, 9],
    [0, 0, 0, 6, 0, 1, 0, 7, 8],
    [0, 0, 7, 0, 4, 0, 2, 6, 0],
    [0, 0, 1, 0, 5, 0, 9, 3, 0],
    [9, 0, 4, 0, 6, 0, 0, 0, 5],
    [0, 7, 0, 3, 0, 0, 0, 1, 2],
    [1, 2, 0, 0, 0, 7, 4, 0, 0],
    [0, 4, 9, 2, 0, 6, 0, 0, 7],
]


def print_grid(grid: list) -> None:
    print()
    length: int = 31
    # Print the top bar
    print("-" * length)
    for row_index, row in enumerate(grid):
        for col_index, num in enumerate(row):
            # Put a bar third entry
            space = "|" if (col_index) % 3 == 0 else ""
            print(f"{space} {num} ", end="")
        # end the line with closing the bar
        print("|")
        # separate the quadrants better with another horizontal bar
        if (row_index + 1) % 3 == 0:
            print("-" * length)
    print()


print_grid(GRID)


def possible(row: int, col: int, d: int) -> bool:
    if d > 9 or d < 1:
        raise ValueError("Only digits from 1 to 9 are allowed to be checked.")
    # Leaving this here in  case we want to make it more modular
    # GRID = GRID
    # If d already exists on that row, it's not possible
    if d in GRID[row]:
        return False
    # If d already exists in that column, it's also not possible
    if any(row_[col] == d for row_ in GRID):
        return False
    # If d is already in that quadrant, then it of course can't
    # be placed
    """
    Quadrants are located like (x, y)
    0,0     1,0     2,0
    0,1     1,1     2,1
    0,2     1,2     2,2
    """
    quad_x: int = col // 3
    quad_y: int = row // 3
    # Just compile the ranges on those rows
    quadrant_entries: list = (
        GRID[quad_y * 3][quad_x * 3 : (quad_x * 3) + 2]
        + GRID[(quad_y * 3) + 1][quad_x * 3 : (quad_x * 3) + 2]
        + GRID[(quad_y * 3) + 2][quad_x * 3 : (quad_x * 3) + 2]
    )
    if d in quadrant_entries:
        return False
    # It is valid!
    return True


# Tests
# print(f"This should return (row) False: \n{possible(0, 1, 1)}", )
# print(f"This should return (col) False: \n{possible(0, 2, 9)}", )
# print(f"This should return (qad) False: \n{possible(7, 4, 3)}", )
# print(f"This should return True: \n{possible(8, 4, 1)}", )
# print(f"This should return False: \n{possible(0, 1, 6)}", )


def solve() -> bool:
    # go through every row
    for row_i, row in enumerate(GRID):
        # go through every column
        for col_i, num in enumerate(row):
            # is the cell in that row/column empty?
            if num == 0:
                # go through digits 1-9
                for try_num in range(1, 10):
                    # can we place that digit here?
                    if possible(row_i, col_i, try_num):
                        # yes -> place digit
                        GRID[row_i][col_i] = try_num
                        if not solve():
                            # if we return from solve to here, that
                            # means the previous placement led to a
                            # dead end undo "place digit"
                            GRID[row_i][col_i] = 0
                # if no digits are possible we have to go back
                return False
    # we made it through every cell without finding and empty one,
    # lets print the grid
    print_grid(GRID)
    return True
    # input("Enter to carry on. Otherwise end.")


solve()
