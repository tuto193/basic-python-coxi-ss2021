def factorial(n: int) -> int:
    if n < 0:
        raise ValueError("Only natural numbers are allowed.")
    # Base case/anchor
    if n == 0:
        return 1
    return n * factorial(n - 1)
