from turtle import forward, right, left, penup, pendown, reset
from math import sqrt

i_0:int = 0
i_1:int = 1
temp:int = 1

end:str = "no"

while end != "yes":
    iterations:int = int(input("Enter the iterations:"))
    angle:float = float(input("Enter the angle:"))
    for n in range(iterations):
            forward(sqrt(i_1))
            right(angle)
            temp = i_0 + i_1
            i_0 = i_1
            i_1 = temp

    end = input("Do you want to end? [yes/no]:")
