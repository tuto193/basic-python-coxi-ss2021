def write_to_file(string, filename):
    """function for writing a string to a file"""
    with open(filename, "+a") as f:
        f.write(string)
        f.write("\n")

template    = "{} is the best color"
colors      = ["light blue", "sky blue", "ocean blue", "navy blue", "dark blue"]

# write a line for each color to the file
for color in colors:
    write_to_file(template.format(color), "best_colors.txt")
