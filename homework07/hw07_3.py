"""Solution for exercise 7.3."""

# The max value in this list is 9
TEST_LIST: list = [4, 9, 2, 4, 1, 1, 5, 1, 0, 0, 9]


def counting_sort(L: list, m: int) -> list:
    """Sort list L, taking max value m into account.

    Args:
        L (list): the list to sort.
        m (int): the highest possible value found within L.

    Returns:
        (list): the sorted L list.
    """
    sorted_list: list = []
    c_list: list = initialize_zero_list(m + 1)
    #  Count how many times each value appears in L
    for value in L:
        c_list[value] += 1
    #  Since each index in c_list is a possible value in L,
    #  each value (of c_list) represents then how many times
    # that index exists in L.
    for index, value in enumerate(c_list):
        for _times in range(value):
            sorted_list.append(index)
    return sorted_list


def initialize_zero_list(m: int) -> list:
    """Initialize a list of length m, with only 0 values.

    Args:
        m (int): length of the desired list.

    Returns:
        list[int]: list of length m with only 0 values.
    """
    return_list: list = []
    for _ in range(m):
        return_list.append(0)
    return return_list
