import numpy as np

first_a = np.arange(3) + 1
second_a = first_a + 5
second_a[0] -= 2

result = np.dot(first_a, second_a)
print(result)
