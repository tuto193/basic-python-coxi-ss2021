import random

file_name = "randomness.txt"

with open(file_name, "w") as f:
    for _ in range(100):
        insert_number = random.randint(0, 9)
        f.write("%s" % insert_number)
    f.write("\n")
