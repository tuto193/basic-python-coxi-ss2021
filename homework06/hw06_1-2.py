# Open file
file = open("my_shadow.txt", "r")

for line in file.readlines():
    print(line, end="")

# Close file
file.close()
