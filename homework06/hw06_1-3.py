from matplotlib import pyplot as plt
list_of_numbers = [-5,-4,-3,-2,-1,0,1,2,3,4,5]
# Your code goes here

squared_numbers = []
for number in list_of_numbers:
    squared_numbers.append(number ** 2)

plt.plot(list_of_numbers, squared_numbers)
plt.show()
