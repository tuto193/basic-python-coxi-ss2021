from csv import DictReader as dr


QUEST_FILE = "06_3_questions_answers.csv"
RESULTS_FILE = "results.csv"

# returns a tuple (bool, str, str)(correct?, player_answer, correct_answer)
def present_question(row):
    print(row["question"])
    print(row["answer a"])
    print(row["answer b"])
    print(row["answer c"])
    player_answer = input().lower()
    if player_answer == row["correct"]:
        print("Correct!")
        return (True, player_answer, player_answer)
    else:
        print("Sorry, that was not the correct answer.")
        return (False, player_answer, row["correct"])


def present_result(n_total, n_correct):
    print("You got %s out of %s correct answers!" % (n_correct, n_total))


# Load questions
with open(QUEST_FILE, newline="") as f:
    questionaire = dr(f)

    # Ask normal stuff
    user_name = input("Hello! What's your name?")
    n_questions = int(input("How many questions would you like to answer?"))

    total_correct = 0
    results = {"question": [], "player answer": [], "correct answer": []}

    current_n = 1

    # as the questions
    for row in questionaire:
        if current_n > n_questions:
            break
        correct, pa, ca = present_question(row)
        total_correct = total_correct + 1 if correct else total_correct
        results["question"] += [row["question"]]
        results["player answer"] += [pa]
        results["correct answer"] += [ca]
        current_n += 1
    if current_n != n_questions + 1:
        print("Sorry. It seems we don't have anymore questions :(.")
        print("We had %s, but you expected %s." % (current_n - 1, n_questions))
    present_result(current_n - 1, total_correct)

    # Save the results in a TSV file
    with open(RESULTS_FILE, "w") as f2:
        # Write the headings first
        f2.write("question,player answer,correct answer\n")
        # Then the actual results
        for i in range(current_n - 1):
            f2.write("%s," % results["question"][i])
            f2.write("%s," % results["player answer"][i])
            f2.write("%s\n" % results["correct answer"][i])
