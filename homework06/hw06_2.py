# 4.1
f_shadow = "06_my_shadow.txt"
f_distances = "06_2_distances.txt"


def files_last_n_lines(file, n):
    f = open(file, "r")
    lines = f.readlines()
    f.close()
    f_len = len(lines)
    for index in range(f_len - n, f_len):
        print(lines[index], end="")

files_last_n_lines(f_shadow, 6)


def add_distances():
    NEW_SUM_FILE = "total_distance.txt"
    NEW_OVER50_FILE = "greater_50.txt"

    # scales
    scale_m = { "m": 1, "cm": 0.01, "mm": 0.001 }
    # Wanted infos
    total = 0
    values_over_50 = ""

    f = open(f_distances, "r")
    f_lines = f.readlines()
    f.close()
    for line in f_lines:
        num, scal = line.strip().split()
        actual_number = float(num) * scale_m[scal]
        total += actual_number
        if actual_number > 50:
            values_over_50 += line
    # 4.3
    f_result = open(NEW_SUM_FILE, "w")
    f_result.write("%s m\n" % total)
    f_result.close()
    # 4.4
    f_over = open(NEW_OVER50_FILE, "w")
    f_over.write(values_over_50)
    f_over.close()

add_distances()
