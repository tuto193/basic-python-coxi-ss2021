from turtle import Screen, Turtle
import random


class DrunkTurtle(Turtle):
    # YOUR CODE HERE
    def __init__(self, angle: int, step_size: int, song: list):
        super().__init__()
        self.angle = angle
        self.step_size = step_size
        self.song = song
        self._current_word = 0

    def sing(self) -> None:
        self.write(self.song[self._current_word])
        self._current_word = (self._current_word + 1) % len(self.song)

    def move(self) -> None:
        turn_angle: int = random.randint(-self.angle, self.angle)
        self.left(turn_angle)
        self.forward(self.step_size)
        self.sing()


screen = Screen()

frank = DrunkTurtle(60, 29, ["foo", "bar", "baz"])  # YOUR CODE HERE
frank.left(180)
pete = DrunkTurtle(42, 20, ["Teenage", "Mutant", "Ninja", "Turtles"])  # YOUR CODE HERE

# dont change anything below this comment


def move(t):
    t.move()
    screen.ontimer(lambda: move(t), 500)


move(frank)
move(pete)
screen.exitonclick()
