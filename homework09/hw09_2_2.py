class Human:
    """Base class for all Humans."""

    def __init__(self, name: str, age: int, phone_number: int):
        self.name = name
        self.age = age
        self.phone_number = phone_number

    def procreate(self, _other_human):
        """Two humans procreate to create a baby (another Human).

        Returns:
            Human: the baby after procreation.
        """
        return Human("Baby Foo", 0, 0)

    def sleep(self, time: float) -> None:
        """Sleep for time amount of hours."""
        pass


class Student(Human):
    """Class defining basic student behaviour."""

    def __init__(
        self,
        name: str,
        age: int,
        phone_number: int,
        student_number: int,
        uni_email: str,
        studies: str,
    ):
        super().__init__(name, age, phone_number)
        self.student_number = student_number
        self.university_email = uni_email
        self.studies = studies

    def study(self, time: float) -> None:
        """Study for anything and everything for time amount of hours."""
        pass

    def party(self) -> None:
        """Party to make sure you can carry on studying without stress."""
        pass


class Employee(Human):
    """Class defining basic employee behaviour."""

    def __init__(
        self,
        name: str,
        age: int,
        phone_number: int,
        salary: int,
        taxpayer_id: int,
        job: str,
    ):
        super().__init__(name, age, phone_number)
        self.salary = salary
        self.taxpayer_id = taxpayer_id
        self.job = job

    def work(self, time: float) -> float:
        """Find out how much is earned in time months based on salary."""
        return self.salary * time

    def pay_taxes(self) -> (float, float):
        """Find out how much salary is actually worth after taxes.

        Returns:
            (float, float): actual money earned, taxes paid.
        """
        pass


test_student: Student = Student("a", 3, 123, 456, "a@uos.de", "Cog.Sci.")
assert test_student.name == "a"
assert test_student.age == 3
assert test_student.phone_number == 123
assert test_student.student_number == 456
assert test_student.university_email == "a@uos.de"
assert test_student.studies == "Cog.Sci."
test_employee: Employee = Employee("b", 4, 321, 654, 69420, "working work")
assert test_employee.name == "b"
assert test_employee.age == 4
assert test_employee.phone_number == 321
assert test_employee.salary == 654
assert test_employee.taxpayer_id == 69420
assert test_employee.job == "working work"
