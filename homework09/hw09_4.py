class Vector2D:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def __add__(self, other_vec):
        return Vector2D(self.x + other_vec.x, self.y + other_vec.y)

    def __str__(self):
        return f"Vector2D({self.x}, {self.y})"


u = Vector2D(12, 36)
v = Vector2D(42, 43)


print(u + v)
