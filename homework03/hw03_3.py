##### 3.1
from turtle import pendown, penup, right, left, forward, reset


def draw_ngon(n: int, sidelength: float = 100) -> None:
    turn_angle = compute_turn_angle(n)
    pendown()
    for _ in range(n):
        forward(sidelength)
        right(turn_angle)
    pendown()


def draw_square(length: float) -> None:
    pendown()
    for _ in range(4):
        forward(length)
        right(90)
    penup()


def compute_turn_angle(n: int) -> float:
    if n <= 0:
        return 0
    return 180 - ((180/n) * (n - 2))


###### 3.2
def is_prime(n: int) -> bool:
    check_limit = n//2
    for i in range(2, check_limit + 1):
        if n % i == 0:
            return False
    return True

range_to_check = int(input("Check until what number? "))
range_to_check = range_to_check if range_to_check > 0 else 1
for i in range(1, range_to_check + 1):
    prime = " " if is_prime(i) else "not "
    print(f"{i} is {prime}prime.")
