dimensions = int(input("Enter the number for the table (nxn):"))

print(" ", end="")
for i in range(1, dimensions + 1):
    print("   ", i, end="")
print()

for left_n in range(1, dimensions + 1):
    print(left_n, end="")
    for top_n in range(1, dimensions + 1):
        ecqxs = "X" if top_n % left_n == 0 else "·"
        print("   ", ecqxs, end="")
    print()
