from random import randint

iterations = int(input("How many digits do you want?"))

i = 0
while i < iterations:
    n = randint(1, 10)
    if n < 4:
        print("O", end="")
    else:
        print("X", end="")
    i += 1
print()
