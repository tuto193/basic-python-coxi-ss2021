# Your grocery list
# items
item_a = 'cheese'
item_b = 'apple'
# prices per unit
price_a = 4.2
price_b = 1.2
# quantities
quantity_a = 2
quantity_b = 5

total_a = price_a * quantity_a
total_b = price_b * quantity_b

total = total_a + total_b

print(f"{item_a} at {price_a} per piece, and {quantity_a} pieces")
print(f" comes to a total of {total_a}")

print(f"{item_b} at {price_b} per piece, and {quantity_b} pieces")
print(f" comes to a total of {total_b}")

print(f"In total you'll have to pay {total}")
