

# Source: https://www.nytimes.com/2018/12/12/technology/amazon-new-york-hq2-data.html

article = """High-Tech Degrees and the Price of an Avocado: The Data New York Gave to Amazon\n\nThe city and state sent loads of data to Amazon during its search for a new headquarters, offering a peek into the valuable information the company collected during the process.\n\nKaren Weise\n\nBy Karen Weise\n\n\tDec. 12, 2018\n\n[Updated Feb. 14: Amazon said it was canceling plans to build a corporate campus in New York City, after the deal had run into fierce opposition from local lawmakers who criticized providing subsidies to one of the world’s most valuable companies.]\n\nAn avocado at Whole Foods costs 1.25. \nColumbia University handed out 724 graduate degrees in computer science over the past three years. \nAnd 10 potential land parcels in Long Island City are zoned M1-4, for light manufacturing.\n\nNew York provided all of these data points, and thousands more, to Amazon as part of its successful bid to woo the tech giant to town.\n\nOn Monday, New York City posted online the 253-page proposal it submitted, along with New York State, to Amazon in March. \nThe city quickly took the file down, saying it should have checked with its partners before posting it, because the document included proprietary information. \nBut The New York Times downloaded the document before it was taken off the public website.\n\nThe proposal shows the types of data, some rarely available publicly, that the company amassed from cities across the country as part of its search for a second headquarters.
"""
def make_nice(article):
    # separate into lines
    article = article.split("\n")

    # remove empty lines and don't add 3rd and 6th lines
    no_empty_ls = ["############################# START OF TEXT #############################"]
    for line in article:
        if line:
            no_empty_ls.append(line)

    no_empty_ls.pop(3)
    no_empty_ls.pop(5)

    more_stuff = []
    for index, line in enumerate(no_empty_ls):
        if index < 5:
            more_stuff.append("%s\n" % line)
        else:
            more_stuff.append("%s: %s" % (index - 4, line))

    more_stuff.append("\n############################## END OF TEXT ##############################")
    # make author + date nicer
    more_stuff[3] = "%s - %s\n" % (no_empty_ls[3].strip(), no_empty_ls[4].strip("\t"))
    # remove unnecessary (repeated) date
    more_stuff.pop(4)
    return "\n".join(more_stuff)

article_nice = make_nice(article)
print(article_nice)

def word_frequency_counter(article) -> dict:
    lower_case_article = article.lower()
    list_of_all_words = lower_case_article.split(" ")
    set_of_words = set(list_of_all_words)
    frequencies = {}
    for word in set_of_words:
        frequencies[word] = list_of_all_words.count(word)
    return frequencies


word_frequencies = word_frequency_counter(article_nice)

mfing_word: str = list(word_frequencies.keys())[0]

for word in word_frequencies.keys():
    if word_frequencies[word] > word_frequencies[mfing_word]:
        mfing_word = word

print("Most frequent word is '%s'" % mfing_word)


def most_frequent_letter(article) -> str:
    char_set = set(article.lower())
    most_frequent_letter = "a"
    for letter in char_set:
        if article.lower().count(letter) > article.lower().count(
            most_frequent_letter
        ):
            most_frequent_letter = letter
    return most_frequent_letter

most_frequent_letter = most_frequent_letter(article_nice)

print("Most frequent char is: '%s'" % most_frequent_letter)


def count_non_alphanum(article) -> int:
    artificially_interted = 118 + 8
    low_art = article.lower()
    alphanum_set = set("abcdefghijklmnopqrstuvwxyz0123456789".split())
    total_chars = 0
    non_alphanum_set = set(low_art) - alphanum_set
    for char in non_alphanum_set:
        total_chars += low_art.count(char)
    return total_chars - artificially_interted

total_non_alphanum = count_non_alphanum(article_nice)
print("Total non-alphanumeric chars in text is: %s" % total_non_alphanum)
