from matplotlib import pyplot as plt

message = """Nhpbz Qbspbz Jhlzhy dhz h Yvthu nlulyhs huk zahalzthu dov wshflk h jypapjhs yvsl pu aol lcluaz aoha slk av aol kltpzl vm aol Yvthu Ylwbispj huk aol ypzl vm aol Yvthu Ltwpyl. Pu 60 IJ, Jhlzhy, Jyhzzbz huk Wvtwlf mvytlk aol Mpyza Aypbtcpyhal, h wvspapjhs hssphujl aoha kvtpuhalk Yvthu wvspapjz mvy zlclyhs flhyz. Aolpy haaltwaz av hthzz wvdly hz Wvwbshylz dlyl vwwvzlk if aol Vwapthalz dpaopu aol Yvthu Zluhal, htvun aolt Jhav aol Fvbunly dpao aol mylxblua zbwwvya vm Jpjlyv. Jhlzhy yvzl av iljvtl vul vm aol tvza wvdlymbs wvspapjphuz pu aol Yvthu Ylwbispj aoyvbno h zaypun vm tpspahyf cpjavyplz pu aol Nhsspj Dhyz, jvtwslalk if 51 IJ, dopjo nylhasf lealuklk Yvthu alyypavyf. Kbypun aopz aptl ol ivao puchklk Iypahpu huk ibpsa h iypknl hjyvzz aol Yopul ypcly. Aolzl hjoplcltluaz huk aol zbwwvya vm opz clalyhu hytf aoylhalulk av ljspwzl aol zahukpun vm Wvtwlf, dov ohk ylhspnulk optzlsm dpao aol Zluhal hmaly aol klhao vm Jyhzzbz pu 53 IJ. Dpao aol Nhsspj Dhyz jvujsbklk, aol Zluhal vyklylk Jhlzhy av zalw kvdu myvt opz tpspahyf jvtthuk huk ylabyu av Yvtl. Slhcpun opz jvtthuk pu Nhbs dvbsk tlhu svzpun opz pttbupaf av jyptpuhs wyvzljbapvu if opz lultplz; ruvdpun aopz, Jhlzhy vwlusf klmplk aol Zluhal'z hbaovypaf if jyvzzpun aol Ybipjvu huk thyjopun avdhykz Yvtl ha aol olhk vm hu hytf. Aopz ilnhu Jhlzhy'z jpcps dhy, dopjo ol dvu, slhcpun opt pu h wvzpapvu vm ulhy bujohsslunlk wvdly huk pumsblujl.""".lower()

##################################################
##### TASK 3 START
##################################################

### Your code goes here
alphabet = "abcdefghijklmnopqrstuvwxyz"
alphabet_set = set(alphabet)


def letter_frequencies(msg) -> dict:
    letter_frequencies = {}
    for letter in alphabet_set:
        letter_frequencies[letter] = msg.count(letter)
    return letter_frequencies


letter_frequencies: dict = letter_frequencies(message)

# plt.bar(list(letter_frequencies.keys()), list(letter_frequencies.values()))
# plt.show()
##################################################
##### TASK 3 END
##################################################

##################################################
##### TASK 4 START
##################################################

# from chart, one can see, that 'l' is the most common letter
# => l = e + shift -> shift = l - e [4]
# shift = l - e OR e - l
# index of e = 4
# index of l = 11
# shift = +7 or -7
# something was wrong in the task. Had to brute force it
shift = -7


def shift_letter(let, shift):
    new_index = alphabet.index(let) + shift
    return alphabet[new_index % len(alphabet)]


# for shift in range(26):
decrypted_message = ""
for letter in message:
    decrypted_letter = letter
    if letter.isalpha():
        decrypted_letter = shift_letter(letter, shift)
    decrypted_message += decrypted_letter

print(decrypted_message, "with shift %s" % shift)
