strings = []
strings.append("the quick brown fox jumps over the lazy dog")
strings.append("   the quick \n brown fox jumps over the lazy dog")
strings.append("the\t\t\tquickbrownfoxjumpsoverthe lazy dog")

def remove_whitespace(m_str) -> str:
    no_space = m_str.split()
    new_string = ""
    for word in no_space:
        new_string += word.strip(" \t\n")
    return new_string

strings[0] = remove_whitespace(strings[0])
strings[1] = remove_whitespace(strings[1])
strings[2] = remove_whitespace(strings[2])
print(strings)
