from random import randint

bottle_name = ""
machine = []

generated_ids = []

def generate_id(generated_ids):

    num1 = randint(0, 9)
    num2 = randint(0, 9)
    num3 = randint(0, 9)
    new_id = "%s%s%s" % (num1, num2, num3)
    new_id = int(new_id)
    for _ in range(100):
        num1 = randint(0, 9)
        num2 = randint(0, 9)
        num3 = randint(0, 9)
        new_id = "%s%s%s" % (num1, num2, num3)
        new_id = int(new_id)
        if new_id not in generated_ids:
            return new_id
    return False


bottle_name = input("Please enter the next bottle's name:\n")
while bottle_name != "finish":
    price = float(input("Thanks. Please enter the price for %s" % bottle_name))
    bottle_id = generate_id(generated_ids)
    machine += [{"name": bottle_name, "price": price, "ID": bottle_id}]
    bottle_name = input("Please enter the next bottle's name:\n")

total = 0
for bottle_dict in machine:
    total += bottle_dict["price"]

print(machine)

print("The total price is ", total)
