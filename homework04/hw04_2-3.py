from random import randint

dimensions_y = int(input("Enter the dimensions for the grid [height]:"))
dimensions_x = int(input("Enter the dimensions for the grid [width]:"))

def make_random_grid(width, height):
    grid = []
    for _ in range(height):
        row_to_add = []
        for _ in range(width):
            row_to_add += [randint(0, 99)]
        grid.append(row_to_add)
    return grid


print(make_random_grid(dimensions_x, dimensions_y))
