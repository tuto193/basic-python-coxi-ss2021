from random import randint

for _ in range(100):
    number_to_print = randint(0, 9)
    print(number_to_print, end="")
print()
