my_list = [2, 3, 9, 1]
print(my_list[1])  # index must be int or slice, not str
my_list[1] = 9
my_list[0] = my_list[1]
print(my_list[:2])
for number in my_list:  # there was a missing ":"
    print(number)  # index out of range (indexing using the values of list)
my_tuple = ("banana", "apple", "cucumber")
print(my_tuple[-1])
# my_tuple[1] = "green apple"  # cannot assign values to tuples after declaration
my_set = {my_tuple[0], my_list[-1]}  # swapped ")" for "}" in the end
# print(my_set)  # set is not subscriptable -> print all?
my_dict = {"tuple": my_tuple, "list": my_list}  # took ":" out of the string
print(my_dict["tuple"])
