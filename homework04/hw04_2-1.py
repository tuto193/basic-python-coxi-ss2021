# 1
fav_music = ("Protest The Hero", "Watsky")
# 2
colors = ("black", "white")
# 3
lectures = (
    ("Advanced Experiment Design in Unity", "Prof. Dr. med. Peter König"),
    ("Computational Creativity", "Dr. Marco Volpe"),
    ("Introduction to Theoretical Computer Science", "Prof. Dr. Markus Chimani")
)
# 4
months = ("January", "February", "March", "April", "May")
# 5
address = ("Baker St.", "221B", "London")
